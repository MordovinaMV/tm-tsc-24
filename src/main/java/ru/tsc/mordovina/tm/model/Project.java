package ru.tsc.mordovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.api.entity.IWBS;
import ru.tsc.mordovina.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractOwnerEntity implements IWBS {

    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @NotNull
    private Status status = Status.NOT_STARTED;
    @Nullable
    private Date startDate;
    @Nullable
    private Date finishDate;
    @NotNull
    private Date created = new Date();

    public Project(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}