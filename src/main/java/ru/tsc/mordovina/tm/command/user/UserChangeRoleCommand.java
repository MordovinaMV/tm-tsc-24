package ru.tsc.mordovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class UserChangeRoleCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-change-role";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Changes user role";
    }

    @Override
    public void execute() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findById(id);
        if (user == null) throw new UserNotFoundException();
        @Nullable final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("Enter role:");
        System.out.println(Arrays.toString(Role.values()));
        @NotNull final String roleValue = TerminalUtil.nextLine();
        @NotNull final Role role = Role.valueOf(roleValue);
        serviceLocator.getUserService().setRole(id, role);
    }

}
