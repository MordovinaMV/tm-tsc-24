package ru.tsc.mordovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class UserRemoveByIdCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-remove-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove user by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId))
            throw new AccessDeniedException();
        serviceLocator.getUserService().removeUserById(id);
    }

}
