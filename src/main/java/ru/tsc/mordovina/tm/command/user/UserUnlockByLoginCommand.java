package ru.tsc.mordovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class UserUnlockByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-unlock-by-login";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final String id = serviceLocator.getUserService().findUserByLogin(login).getId();
        @Nullable final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

}
